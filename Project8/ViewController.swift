//
//  ViewController.swift
//  Project8
//
//  Created by Vrushali Kulkarni on 09/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {

    var scoreLabel: UILabel!
    var cluesLabel: UILabel!
    var answersLabel: UILabel!
    var currentAnswer: UITextField!
    var lettersButton = [UIButton]()
    var submit: UIButton!
    var clear: UIButton!
    var buttonsView: UIView!

    var activateButtons = [UIButton]()
    var solutions = [String]()
    var score = 0 {
        didSet {
            self.scoreLabel.text = "Score: \(score)"
        }
    }
    var level = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        loadLevel()
    }

    override func loadView() {
        configureUIComponents()
        addConstraints()
        configureLettersButton()
    }
}

private extension ViewController {

    func configureUIComponents() {

        view = UIView()
        view.backgroundColor = .white

        scoreLabel = UILabel()
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        scoreLabel.textAlignment = .right
        scoreLabel.text = "Score:0"
        view.addSubview(scoreLabel)

        cluesLabel = UILabel()
        cluesLabel.translatesAutoresizingMaskIntoConstraints = false
        cluesLabel.text = "CLUES"
        cluesLabel.numberOfLines = 0
        cluesLabel.font = UIFont.systemFont(ofSize: 24)
        cluesLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        view.addSubview(cluesLabel)

        answersLabel = UILabel()
        answersLabel.translatesAutoresizingMaskIntoConstraints = false
        answersLabel.text = "ANSWERS"
        answersLabel.textAlignment = .right
        answersLabel.numberOfLines = 0
        answersLabel.font = UIFont.systemFont(ofSize: 24)
        answersLabel.textAlignment = .right
        answersLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        view.addSubview(answersLabel)

        currentAnswer = UITextField()
        currentAnswer.translatesAutoresizingMaskIntoConstraints = false
        currentAnswer.placeholder = "Tap here to guess"
        currentAnswer.textAlignment = .center
        currentAnswer.font = UIFont.systemFont(ofSize: 44)
        currentAnswer.isUserInteractionEnabled = false
        view.addSubview(currentAnswer)

        submit = UIButton(type: .system)
        submit.translatesAutoresizingMaskIntoConstraints = false
        submit.setTitle("SUBMIT", for: .normal)
        submit.addTarget(self, action: #selector(submitTapped), for: .touchUpInside)
        view.addSubview(submit)

        clear = UIButton(type: .system)
        clear.translatesAutoresizingMaskIntoConstraints = false
        clear.setTitle("CLEAR", for: .normal)
        clear.addTarget(self, action: #selector(clearTapped), for: .touchUpInside)
        view.addSubview(clear)

        buttonsView = UIView()
        buttonsView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonsView)
    }

    func addConstraints() {

        NSLayoutConstraint.activate([
            scoreLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            scoreLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),

            cluesLabel.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor),
            cluesLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor, constant: 100),
            cluesLabel.widthAnchor.constraint(equalTo: view.layoutMarginsGuide.widthAnchor, multiplier: 0.6, constant: -100),

            answersLabel.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor),
            answersLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor, constant: -100),
            answersLabel.widthAnchor.constraint(equalTo: view.layoutMarginsGuide.widthAnchor, multiplier: 0.4, constant: -100),
            answersLabel.heightAnchor.constraint(equalTo: cluesLabel.heightAnchor),

            currentAnswer.topAnchor.constraint(equalTo: cluesLabel.bottomAnchor, constant: 40),
            currentAnswer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            currentAnswer.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),

            submit.topAnchor.constraint(equalTo: currentAnswer.bottomAnchor),
            submit.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -100),
            submit.heightAnchor.constraint(equalToConstant: 44),

            clear.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 100),
            clear.centerYAnchor.constraint(equalTo: submit.centerYAnchor),
            clear.heightAnchor.constraint(equalToConstant: 44),

            buttonsView.widthAnchor.constraint(equalToConstant: 750),
            buttonsView.heightAnchor.constraint(equalToConstant: 320),
            buttonsView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            buttonsView.topAnchor.constraint(equalTo: submit.bottomAnchor, constant: 20),
            buttonsView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -20)
        ])
    }

    func configureLettersButton() {

        let width = 150
        let height = 80

        for row in 0..<4 {
            for col in 0..<5 {

                let letterButton = UIButton(type: .system)
                letterButton.titleLabel?.font = UIFont.systemFont(ofSize: 36)
                letterButton.setTitle("WWW", for: .normal)
                letterButton.addTarget(self, action: #selector(letterTapped), for: .touchUpInside)
                letterButton.frame = CGRect(x: col*width, y: row*height, width: width, height: height)
                letterButton.layer.borderWidth = 1.0
                letterButton.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0).cgColor
                buttonsView.addSubview(letterButton)
                lettersButton.append(letterButton)
            }
        }
    }
}

extension ViewController {

    @objc func clearTapped() {

        currentAnswer.text = ""
        for button in activateButtons {
            button.isHidden = false
        }
        activateButtons.removeAll()
    }

    @objc func submitTapped() {

        guard let answerText = currentAnswer.text else {
            print("There is no answer to submit")
            return
        }

        guard let solutionPosition = solutions.firstIndex(of: answerText) else {
            showAlertForIncorrectguess()
            score -= 1
            return
        }

        activateButtons.removeAll()

        var splitAnswers = answersLabel.text?.components(separatedBy: "\n")
        splitAnswers?[solutionPosition] = answerText
        answersLabel.text = splitAnswers?.joined(separator: "\n")

        currentAnswer.text = ""
        score += 1

        if score % 7 == 0 {
            showAlertForNextLevel()
        }
    }

    @objc func letterTapped(_ sender: UIButton) {

        guard let title = sender.titleLabel?.text else { return }
        currentAnswer.text = currentAnswer.text?.appending(title)
        activateButtons.append(sender)
        sender.isHidden = true
    }

    func showAlertForNextLevel() {

        let alertController = UIAlertController(title: "Well done!",
                                                message: "Are you ready for next level?",
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nextLevelTapped)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }

    func showAlertForIncorrectguess() {

        let alertController = UIAlertController(title: "Oops Incorrect guess",
                                                message: "Please try another one!",
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self] _ in
            self?.clearTapped()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }

    func nextLevelTapped(_ alertAction: UIAlertAction) {
        level += 1
        solutions.removeAll(keepingCapacity: true)
        loadLevel()

        for button in lettersButton {
            button.isHidden = false
        }
    }

    func loadLevel() {
        var clue = ""
        var hint = ""
        var letterBits = [String]()

        guard let levelFileURL = Bundle.main.path(forResource: "level\(level)", ofType: ".txt") else {
            print("File level\(level) not found")
            return
        }

        guard let levelContents = try? String(contentsOfFile: levelFileURL) else {
            print("Contents of file level\(level) not found")
            return
        }

        var lines = levelContents.components(separatedBy: "\n")
        lines.shuffle()
        for(index, line) in lines.enumerated() {
            let part = line.components(separatedBy: ": ")
            let cluePart = part[1]
            let answer = part[0]
            clue += "\(index + 1). \(cluePart)\n"
            let solutionWord = answer.replacingOccurrences(of: "|", with: "")
            hint += "\(solutionWord.count) letters\n"
            solutions.append(solutionWord)
            let bits = answer.components(separatedBy: "|")
            letterBits += bits
        }

        cluesLabel.text = clue.trimmingCharacters(in: .whitespacesAndNewlines)
        answersLabel.text = hint.trimmingCharacters(in: .whitespacesAndNewlines)

        lettersButton.shuffle()

        guard lettersButton.count == letterBits.count else {
            print("Letter buttons's count is not equal to letter bit's count")
            return
        }

        for i in 0 ..< lettersButton.count {
            lettersButton[i].setTitle(letterBits[i], for: .normal)
        }
    }
}
